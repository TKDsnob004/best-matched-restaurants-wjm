# Best Matched Restaurants WJM
This service handles the registration of cuisine and restaurant csv files and allows for querying for restaurants given a series of REST endpoints. Please read the installation and environment setup below. 

# Environment Setup

# Installation List
1.  [IntelliJ - Java IDE](#intellij).
2.  [Postman - Rest HTTP Client Software](#postman)
3.  [JDK - Java Development Kit](#jdk)
4.  [Git Bash - Command tool](#git-bash)
5.  [Running the Application](#running-the-application)

## IntelliJ
IntelliJ is the IDE I used to develop this service. However, any IDE can be utilized. Download the latest version from [here](https://www.jetbrains.com/idea/download/#section=windows). Be sure to download the executable file. Follow the installation prompts.

## Postman
Postman is a powerful HTTP Client service. This application will be using REST and as a result, Postman allows for someone to hit REST Endpoints. Postman can be downloaded [here](https://www.postman.com/downloads/). Follow the standard installation settings.

## JDK
JDK is going to be your JAVA Development Tool. Download the latest open jdk [JDK](https://jdk.java.net/java-se-ri/13). 

## Git Bash 
 Download git from [here](https://git-scm.com/downloads).  
 Once downloaded you will need to create **.bashrc** and **.bashrc_profile** files. 
 
### Creating SSH Keys
Once you have downloaded git bash you will need to generate a RSA key pair and add it to your Git Lab profile settings. This key pair will allow you to clone and edit repositories via the command line. (Absolutely necessary.)  
[Follow these steps on how to create a SSH token](https://docs.gitlab.com/ee/ssh/#generating-a-new-ssh-key-pair).    
[Follow these steps to add that ssh token to git lab](https://docs.gitlab.com/ee/ssh/#adding-an-ssh-key-to-your-gitlab-account)
 
### .bashrc
You will need to add the following to your bashrc file:  
```viml
export JAVA_HOME='C:/Users/{userFolder}/jdk-11.0.6.10' <- wherever your JDK was downloaded too
export GRADLE_USER_HOME='C:/Users/{userFolder}/.gradle' <- wherever your .gradle folder was downloaded too
export NO_PROXY=localhost,127.0.0.1
export no_proxy=localhost,127.0.0.1
```

### Running the Application
To run the application is pretty simple. Run the following command:
```viml
./gradlew.bat quarkusDev -x buildNative
```
Once the application has started up, logs will display showing the `*.csv` files have been successfully registered.   The services default port is `10103`. However, this can be changed by going to `src/main/resources/application.yaml` and updating the `%dev.quarkus.http.port` value. From there you can utilize `Postman` to use the endpoints exposed [here](src/main/resources/openapi.yaml).

| config property | description | default value | 
| ------ | ------ | ------ |
| finder.cuisine-location | the location of the file to be used loading the `cuisines.csv` file | `/csv/cuisines.csv` |
| finder.restaurants-location | the location of the file to be used loading the `restaurants.csv` file | `/csv/restaurants.csv` |
| finder.max-items | the max number of restaurants to be queried | `5` |
| %dev.quarkus.http.port | The port the application runs on | `10103` |

You can also test the application run running the included unit tests/adding your own. The `Query.Builder` object makes it easy to create, then subsequently query a list of restaurants. 
 
### Next steps (What I want to implement)
I created a `RestaurantRegistry` and `RestaurantProvider` as opposed to connecting to a database for simplicities’ sake. I would like to eventually make a database structure and store this data directly there. I would do this by utilizing flyway and connecting to a postgres database ([see here](https://quarkus.io/guides/flyway)). This would automatically create the schema and table structures required.

Furthermore, I would like to create an interface annotation that automatically registers a `CsvRow` to a mapped set of string values. OR Create a custom set of rows that allows me to grab a value based on a column name. This removes a dependency on the ordering of columns. This is due to the fact that I do not like the static constructors while maintaining readability and json de/serialization.   

[to top](#environment-setup)