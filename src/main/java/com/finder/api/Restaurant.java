package com.finder.api;

import static com.finder.utilities.CommonParameters.requireGreaterThanEqualTo;
import static com.finder.utilities.CommonParameters.requireNonNullNoWhiteSpace;
import static com.finder.utilities.CommonParameters.requireBetween;

import com.finder.utilities.CommonParameters;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbPropertyOrder;
import java.util.Objects;

/**
 * A {@link Restaurant} containing the information needed for a Restaurant
 */
@JsonbPropertyOrder({ "name", "customer_rating", "distance", "price", "cuisine_id" })
public class Restaurant implements CsvRow {
    private static final String NAME_KEY = "name";
    private static final String RATING_KEY = "customer_rating";
    private static final String DISTANCE_KEY = "distance";
    private static final String PRICE_KEY = "price";
    private static final String CUISINE_KEY = "cuisine_id";

    private final String name;
    private final int rating;
    private final double distance;
    private final double price;
    private final long cuisineId;

    /**
     * Json-b Constructor
     * @param name the name of the restaurant
     * @param rating the rating for the restaurant
     * @param distance the distance to the restaurant (in miles)
     * @param price the average price of the restaurant
     * @param cuisineId the cuisine type for the restaurant
     */
    @JsonbCreator
    public Restaurant(@JsonbProperty(NAME_KEY) final String name, @JsonbProperty(RATING_KEY) final int rating,
                      @JsonbProperty(DISTANCE_KEY) final double distance, @JsonbProperty(PRICE_KEY) final double price,
                      @JsonbProperty(CUISINE_KEY) final long cuisineId) {
        this.name = requireNonNullNoWhiteSpace(name, "a name is required");
        this.rating = requireBetween(rating, 1, 5);
        this.distance = requireGreaterThanEqualTo(distance, 0d);
        this.price = requireGreaterThanEqualTo(price, 0d);
        this.cuisineId = requireGreaterThanEqualTo(cuisineId, 0);
    }

    /**
     * Reads a string array and converts the values to a {@link Restaurant}
     * @param values the array of string values
     * @return a newly constructed {@link Restaurant}
     */
    public static Restaurant read(final String[] values) {
        CommonParameters.requireWithinRange(values, 5);
        final String name = values[0];
        final int rating = Integer.parseInt(values[1]);
        final double distance = Double.parseDouble(values[2]);
        final double price = Double.parseDouble(values[3]);
        final long cuisineId = Long.parseLong(values[4]);

        return new Restaurant(name, rating, distance, price, cuisineId);
    }

    /**
     * @return the name of the restaurant
     */
    public String getName() {
        return name;
    }

    /**
     * @return the average customer rating
     */
    @JsonbProperty("customer_rating")
    public int getRating() {
        return rating;
    }

    /**
     * @return the distance to the restaurant
     */
    public double getDistance() {
        return distance;
    }

    /**
     * @return the average price of the restaurant
     */
    public double getPrice() {
        return price;
    }

    /**
     * @return the mapped cuisine
     */
    @JsonbProperty("cuisine_id")
    public long getCuisineId() {
        return cuisineId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Restaurant)) {
            return false;
        }
        Restaurant that = (Restaurant) o;
        return rating == that.rating && Double.compare(that.distance, distance) == 0
                && Double.compare(that.price, price) == 0 && cuisineId == that.cuisineId
                && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, rating, distance, price, cuisineId);
    }

    @Override
    public String toString() {
        return "Restaurant: [name=" + name + ", rating=" + rating + ", avgPrice=" + price + ']';
    }
}
