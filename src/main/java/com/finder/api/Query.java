package com.finder.api;

import com.finder.utilities.CommonParameters;
import io.smallrye.mutiny.Uni;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Queries a list of {@link RestaurantWithCuisine} given provided filter criteria
 */
public final class Query {

    /**
     * Sorts a list of {@link RestaurantWithCuisine} by distance (closest -> furthest),
     *                                               then by number of stars (higher -> lower)
     *                                               then by price (lowest -> highest)
     *                                               and finally by the name of the mapped cuisine
     */
    private static final Comparator<RestaurantWithCuisine> COMPARATOR = Comparator.comparingDouble(RestaurantWithCuisine::getDistance)
                                                                                 .thenComparing(Comparator.comparingInt(RestaurantWithCuisine::getRating).reversed())
                                                                                 .thenComparingDouble(RestaurantWithCuisine::getPrice)
                                                                                 .thenComparing(RestaurantWithCuisine::getCuisineName);

    private final String restaurantName;
    private final Integer customerRating;
    private final Double distance;
    private final Double averagePrice;
    private final List<Cuisine> cuisines;

    /**
     * Constructor
     * @param builder the query builder used
     */
    private Query(final QueryBuilder builder) {
        this.restaurantName = builder.restaurantName;
        this.customerRating = CommonParameters.allowNullOrBetween(builder.customerRating, 1, 5);
        this.distance = CommonParameters.allowNullOrGreaterThan(builder.distance, 0d);
        this.averagePrice = CommonParameters.allowNullOrGreaterThan(builder.averagePrice, 0d);
        this.cuisines = builder.cuisines;
    }

    /**
     * @return creates a new {@link QueryBuilder}
     */
    public static QueryBuilder create() {
        return new QueryBuilder();
    }

    /**
     * @return creates an empty {@link Query}
     */
    public static Query empty() {
        return new QueryBuilder().build();
    }

    /**
     * Queries a list of {@link RestaurantWithCuisine} based on the provided query parameters
     * @param maxItems the maximum number of items to be supplied. If limit == 0, there is no limit
     * @param restaurants the listing of {@link RestaurantWithCuisine}
     * @return the filtered, sorted and limited results
     */
    public List<RestaurantWithCuisine> query(final long maxItems, final List<RestaurantWithCuisine> restaurants) {
        final Predicate<RestaurantWithCuisine> predicate = buildPredicate();
        Stream<RestaurantWithCuisine> limited = restaurants.stream().filter(predicate).sorted(COMPARATOR);

        if (maxItems > 0) {
            limited = limited.limit(maxItems);
        }

        return limited.collect(Collectors.toList());
    }

    /**
     * Builds the predicate to be used when filtering the list of restaurants
     * @return the predicate
     */
    private Predicate<RestaurantWithCuisine> buildPredicate() {
        Predicate<RestaurantWithCuisine> builder = Predicate.not(Objects::isNull);

        if (this.distance != null) {
            // Distance has been provided. Add a query for any distances less than or equal to the provided one
            builder = builder.and(r -> this.distance.compareTo(r.getDistance()) >= 0);
        }

        if (this.restaurantName != null && !this.restaurantName.isEmpty()) {
            // A restaurant name has been provided. Return only the results whose name contains the provided information
            builder = builder.and(r -> r.getName().contains(this.restaurantName));
        }

        if (this.customerRating != null) {
            // A customer rating has been provided. Return only the results with ratings greater than or equal to the provided rating.
            builder = builder.and(r -> this.customerRating.compareTo(r.getRating()) <= 0);
        }

        if (this.averagePrice != null) {
            // Price has been provided. Add a query for any Price less than or equal to the provided one
            builder = builder.and(r -> this.averagePrice.compareTo(r.getPrice()) >= 0);
        }

        if (!this.cuisines.isEmpty()) {
            // Cuisine types have been provided. Ensure only matching cuisines are returned
            Set<Long> ids = this.cuisines.stream().map(Cuisine::getId).collect(Collectors.toSet());
            builder = builder.and(r -> ids.contains(r.getCuisineId()));
        }

        return builder;
    }

    /**
     * Builds a {@link Query} based on passed in query parameters
     */
    public static class QueryBuilder {
        private String restaurantName;
        private Integer customerRating;
        private Double distance;
        private Double averagePrice;
        private final List<Cuisine> cuisines;

        /**
         * Constructor
         */
        private QueryBuilder() {
            this.cuisines = new ArrayList<>();
        }

        /**
         * @return builds a new {@link Query}
         */
        public Query build() {
            return new Query(this);
        }

        /**
         * Adds a restaurant name to the parameters
         * @param name the restaurant name
         * @return this builder
         */
        public QueryBuilder setRestaurantName(final String name) {
            this.restaurantName = name;
            return this;
        }

        /**
         * Adds a customer rating to the parameters
         * @param rating the customer rating
         * @return this builder
         */
        public QueryBuilder setCustomerRating(final Integer rating) {
            this.customerRating = rating;
            return this;
        }

        /**
         * Adds a desired distance to the parameters
         * @param totalDistance the desired distance
         * @return this builder
         */
        public QueryBuilder setDistance(final Double totalDistance) {
            this.distance = totalDistance;
            return this;
        }

        /**
         * Adds an average price to the parameters
         * @param price the average price
         * @return this builder
         */
        public QueryBuilder setAveragePrice(final Double price) {
            this.averagePrice = price;
            return this;
        }

        /**
         * Adds a single cuisine to the to the parameters
         * @param cuisine the mapped cuisine
         * @return this builder
         */
        public QueryBuilder addCuisine(final Cuisine cuisine) {
            this.cuisines.add(cuisine);
            return this;
        }

        /**
         * Adds a collection of cuisines to the parameters
         * @param cuisines the collection of cuisines
         * @return this builder
         */
        public QueryBuilder addAllCuisines(final Collection<Cuisine> cuisines) {
            this.cuisines.addAll(cuisines);
            return this;
        }
    }
}
