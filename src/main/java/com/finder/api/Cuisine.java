package com.finder.api;

import com.finder.utilities.CommonParameters;

import javax.json.bind.annotation.JsonbCreator;
import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbPropertyOrder;
import java.util.Objects;

/**
 * A {@link CsvRow} containing the information needed for a cuisine
 */
@JsonbPropertyOrder({ "id", "name" })
public class Cuisine implements CsvRow {
    private final long id;
    private final String name;

    /**
     * Json-b constructor
     * @param id the unique id of the cuisine
     * @param name the name of the cuisine
     */
    @JsonbCreator
    public Cuisine(@JsonbProperty("id") final long id, @JsonbProperty("name") final String name) {
        this.id = CommonParameters.requireGreaterThanEqualTo(id, 0);
        this.name = CommonParameters.requireNonNullNoWhiteSpace(name, "name is required");
    }

    /**
     * Reads a string array and converts the values to a {@link Cuisine}
     * @param values the array of string values
     * @return a newly constructed {@link Cuisine}
     */
    public static Cuisine read(final String[] values) {
        CommonParameters.requireWithinRange(values, 2);
        final long id = Long.parseLong(values[0]);
        final String name = values[1];

        return new Cuisine(id, name);
    }

    /**
     * @return the unique id of the cuisine
     */
    public long getId() {
        return this.id;
    }

    /**
     * @return the name of the cuisine
     */
    public String getName() {
        return this.name;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cuisine)) {
            return false;
        }
        Cuisine cuisine = (Cuisine) o;
        return id == cuisine.id &&  Objects.equals(name, cuisine.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return "Cuisine: [" + name + "]";
    }
}
