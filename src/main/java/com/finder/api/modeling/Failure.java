package com.finder.api.modeling;

import com.finder.utilities.CommonParameters;

import java.util.Objects;

/**
 * Designates a failure
 */
public class Failure {
    /**
     * No failure
     */
    public static final Failure NO_FAILURE = new Failure("NoFailure");

    private final String code;

    /**
     * Constructor
     * @param code the reason for the failure
     */
    public Failure(final String code) {
        this.code = CommonParameters.requireNonNullNoWhiteSpace(code, "a failure code is required");
    }

    /**
     * @return the failure code
     */
    public String getCode() {
        return code;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Failure)) {
            return false;
        }
        Failure failure = (Failure) o;
        return Objects.equals(code, failure.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }
}
