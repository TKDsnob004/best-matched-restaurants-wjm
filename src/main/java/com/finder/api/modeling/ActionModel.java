package com.finder.api.modeling;

import javax.json.bind.annotation.JsonbPropertyOrder;
import java.io.Serializable;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Models an action this service undergoes
 * @param <T> the type of modeled object
 */
@JsonbPropertyOrder({ "success", "item", "failure" })
public class ActionModel<T> implements Serializable {
    T item;
    Failure failure;

    /**
     * Private constructor
     * @param item the item to be used for the modeling
     * @param failure the failure reason
     */
    private ActionModel(final T item, final Failure failure) {
        this.item = item;
        this.failure = Objects.requireNonNull(failure, "a failure is required");
    }

    /**
     * Creates a successful model
     * @param item the item being modeled
     * @param <R> the type of item being modeled
     * @return a new {@link ActionModel} with the designated item and no failure reason set
     */
    public static <R> ActionModel<R> success(final R item) {
        return new ActionModel<>(item, Failure.NO_FAILURE);
    }

    /**
     * Creates a failed model
     * @param failure the reason for the failure
     * @param <R> the expected type of item (will be {@code null})
     * @return a failed model
     */
    public static <R> ActionModel<R> failure(final Failure failure) {
        return new ActionModel<>(null, failure);
    }

    /**
     * Creates a failed model
     * @param throwable the reason for the failure
     * @param <R> the expected type of item (will be {@code null})
     * @return a failed model
     */
    public static <R> ActionModel<R> failure(final Throwable throwable) {
        return new ActionModel<>(null, new Failure(throwable.getLocalizedMessage()));
    }

    /**
     * @return {@code true} if there was no failure. {@code false} otherwise
     */
    public boolean isSuccess() {
        return this.failure == Failure.NO_FAILURE;
    }

    /**
     * @return {@code true} if the model has an item. {@code false} otherwise
     */
    public boolean hasItem() {
        return this.item != null;
    }

    /**
     * @return the modeled item
     */
    public T getItem() {
        return item;
    }

    /**
     * @return the reason for the failure
     */
    public Failure getFailure() {
        return failure;
    }

    /**
     * Maps the modeled object based on a supplied mapping function.
     * If the model has failed for any reason, the mapped function is ignored and the failure reason is persisted
     * @param mapper the mapping function that converts the modeled item to a different object
     * @param <R> the object this item needs to be converted too.
     * @return the converted model based on the mapping function
     */
    public <R> ActionModel<R> map(final Function<T, R> mapper) {
        if (isSuccess()) {
            return ActionModel.success(mapper.apply(this.item));
        }
        return ActionModel.failure(this.failure);
    }
    
    public ActionModel<T> invoke(final Runnable runnable) {
        return this.invoke(ignored -> runnable.run());
    }

    /**
     * Invokes a consumer if the model has succeeded.
     * @param consumer the consumer to be invoked
     * @return this model
     */
    public ActionModel<T> invoke(final Consumer<T> consumer) {
        if (isSuccess()) {
            consumer.accept(this.item);
        }
        return this;
    }

    /**
     * Invokes a bi-consumer given a key mapping function if the model has succeeded.
     * @param keyMapper the mapping function to be used to convert this item to a separate key.
     *                  This key is then used in the bi-consumer
     * @param biConsumer the bi-consumer to be utilized if this model has succeeded
     * @param <K> the key object
     * @return this model
     */
    public <K> ActionModel<T> invoke(final Function<T, K> keyMapper, final BiConsumer<K, T> biConsumer) {
        if (isSuccess()) {
            final K key = keyMapper.apply(this.item);
            biConsumer.accept(key, this.item);
        }
        return this;
    }
}
