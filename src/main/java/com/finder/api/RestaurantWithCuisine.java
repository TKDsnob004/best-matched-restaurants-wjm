package com.finder.api;

import java.util.Objects;

/**
 * A class designating a restaurant has been successfully designated to a cuisine
 */
public class RestaurantWithCuisine extends Restaurant {

    private final Cuisine cuisine;

    /**
     * Constructor
     * @param restaurant the mapped restaurant
     * @param cuisine the mapped cuisine for the restaurant
     */
    public RestaurantWithCuisine(final Restaurant restaurant, final Cuisine cuisine) {
        this(restaurant.getName(), restaurant.getRating(), restaurant.getDistance(), restaurant.getPrice(),
             restaurant.getCuisineId(), cuisine);
    }

    /**
     * Private constructor
     *
     * @param name      the name of the restaurant
     * @param rating    the rating for the restaurant
     * @param distance  the distance to the restaurant
     * @param price     the average price of the restaurant
     * @param cuisineId the cuisine type for the restaurant
     * @param cuisine   the mapped cuisine
     */
    private RestaurantWithCuisine(final String name, final int rating, final double distance, final double price,
                                 final long cuisineId, final Cuisine cuisine) {
        super(name, rating, distance, price, cuisineId);
        Objects.requireNonNull(cuisine, "a cuisine is required");

        if (cuisineId != cuisine.getId()) {
            throw new IllegalArgumentException("The cuisine id's must match");
        }

        this.cuisine = cuisine;
    }

    /**
     * @return the name of the cuisine
     */
    public String getCuisineName() {
        return this.cuisine.getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RestaurantWithCuisine)) {
            return false;
        }
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), cuisine);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
