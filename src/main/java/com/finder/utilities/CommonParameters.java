package com.finder.utilities;

import com.finder.api.modeling.Failure;
import com.finder.api.modeling.ActionModel;

import java.util.Arrays;

/**
 * A common parameters utilization class
 */
public final class CommonParameters {

    /**
     * Requires a string be not null and not empty.
     * @param value the value to be checked
     * @param message the message to be displayed given a failure
     * @return the value if it passes its checks. Otherwise a {@link IllegalArgumentException} is thrown
     */
    public static String requireNonNullNoWhiteSpace(final String value, final String message) {
        if (value == null || value.isEmpty()) {
            throw new IllegalArgumentException(message);
        }
        return value;
    }

    /**
     * Requires a long value be greater than or equal to another long
     * @param value the value to be checked
     * @param comp the max value
     * @return the value if it passes its checks. Otherwise a {@link IllegalArgumentException} is thrown
     */
    public static long requireGreaterThanEqualTo(final long value, final long comp) {
        if (value < comp) {
            throw new IllegalArgumentException(greaterThanMessage(comp));
        }
        return value;
    }

    /**
     * Requires a double value be greater than or equal to another double
     * @param value the value to be checked
     * @param comp the max value
     * @return the value if it passes its checks. Otherwise a {@link IllegalArgumentException} is thrown
     */
    public static double requireGreaterThanEqualTo(final double value, final double comp) {
        if (Double.compare(value, comp) < 0) {
            throw new IllegalArgumentException(greaterThanMessage(comp));
        }
        return value;
    }

    /**
     * Requires an integer value be greater than or equal to another integer
     * @param value the value to be checked
     * @param comp the max value
     * @return the value if it passes its checks. Otherwise a {@link IllegalArgumentException} is thrown
     */
    public static int requireGreaterThanEqualTo(final int value, final int comp) {
        if (value < comp) {
            throw new IllegalArgumentException(greaterThanMessage(comp));
        }
        return value;
    }

    /**
     * Requires an integer value is between two other integer values
     * @param value the value to be checked
     * @param min the min allowed value
     * @param max the max allowed value
     * @return the value if it passes its checks. Otherwise a {@link IllegalArgumentException} is thrown
     */
    public static int requireBetween(final int value, final int min, final int max) {
        if (value < min || value > max) {
            throw new IllegalArgumentException("The value must be between " + min + " and " + max);
        }
        return value;
    }

    /**
     * Requires an integer value be either null or between two other integer values
     * @param value the value to be checked
     * @param min the min allowed value
     * @param max the max allowed value
     * @return the value if it passes its checks. Otherwise a {@link IllegalArgumentException} is thrown
     */
    public static Integer allowNullOrBetween(final Integer value, final int min, final int max) {
        if (value != null && (value < min || value > max)) {
            throw new IllegalArgumentException("The value must be null or between " + min + " and " + max);
        }
        return value;
    }

    /**
     * Requires a double value be null or greater than another double value
     * @param value the value to be checked
     * @param max the max allowed value
     * @return the value if it passes its checks. Otherwise a {@link IllegalArgumentException} is thrown
     */
    public static Double allowNullOrGreaterThan(final Double value, final double max) {
        if (value != null && value.compareTo(max) < 0) {
            throw new IllegalArgumentException("The value must be null or greater than " + max);
        }
        return value;
    }

    /**
     * Requires the length of an array be within a provided range
     * @param values the array
     * @param maxValues the max allowed items
     * @param <T> the type of array
     * @return the copy of the array. Otherwise a {@link IllegalArgumentException} is thrown
     */
    public static <T> T[] requireWithinRange(final T[] values, final int maxValues) {
        if (values.length <= maxValues) {
            return Arrays.copyOf(values, values.length);
        }

        throw new IllegalArgumentException("The array cannot contain more than " + maxValues + " items");
    }

    /**
     * Requires an item be non null. Otherwise a failed model is returned
     * @param item the item to be checked
     * @param code the code to be used on failure
     * @param <T> the type of object
     * @return the item modeled
     */
    public static <T> ActionModel<T> notNullElseFail(final T item, final String code) {
        if (item == null) {
            return ActionModel.failure(new Failure(code));
        }
        return ActionModel.success(item);
    }

    /**
     * Returns a greater than or equal to failure message
     * @param number the number
     * @return the message
     */
    private static String greaterThanMessage(final Number number) {
        return "The value must be greater than or equal to " + number;
    }

    /**
     * Constructor
     */
    private CommonParameters() {
        // empty
    }
}
