package com.finder.utilities.csv;

import com.finder.api.CsvRow;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Reads an input stream and converts it to a {@link CsvRow}.
 * TODO: jackson has a CsvMapper object that converts a CsvFile to a json object.
 *       However, this json object is NOT a javax.json object (what this application uses).
 */
public final class CsvFileReader {
    private static final Pattern PATTERN = Pattern.compile(",");

    /**
     * Reads an input stream and apply the conversion function
     * @param stream the input stream
     * @param converter the converter to be used on each line
     * @param <T> the type of {@link CsvRow} to be converted
     * @return a list of converted {@link CsvRow}
     */
    public static <T extends CsvRow> List<T> read(final InputStream stream, final Function<String[], T> converter) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

        return reader.lines()
                     .skip(1) // Skip the first line (the columns)
                     .map(PATTERN::split) // Split the columns based on a comma
                     .map(converter) // Apply the converter (converts a string array to a CsvBody object)
                     .collect(Collectors.toList());
    }

    /**
     * Default constructor
     */
    private CsvFileReader() {
        // Empty on purpose
    }
}
