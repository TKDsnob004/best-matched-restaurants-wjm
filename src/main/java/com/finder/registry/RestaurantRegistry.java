package com.finder.registry;

import com.finder.api.CsvRow;
import com.finder.api.Cuisine;
import com.finder.api.Restaurant;
import com.finder.api.RestaurantWithCuisine;
import com.finder.api.modeling.Failure;
import com.finder.api.modeling.ActionModel;
import com.finder.utilities.CommonParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Holds the registered mapping of {@link Cuisine} and listing of {@link RestaurantWithCuisine}
 */
class RestaurantRegistry {
    private static final Logger LOGGER = LoggerFactory.getLogger(RestaurantRegistry.class);

    final Map<Long, Cuisine> cuisineRegistry;
    final List<RestaurantWithCuisine> restaurants;

    /**
     * Constructor
     */
    RestaurantRegistry() {
        this.cuisineRegistry = new HashMap<>();
        this.restaurants = new ArrayList<>();
    }

    /**
     * Registers a collection of {@link Cuisine} with this registry
     * @param cuisines the collection of cuisines
     * @return the list of modeled cuisines
     */
    List<ActionModel<Cuisine>> registerCuisines(final Collection<Cuisine> cuisines) {
        return cuisines.stream().map(this::registerCuisine).collect(Collectors.toList());
    }

    /**
     * Registers a single {@link Cuisine} with this registry
     * @param cuisine the cuisine to be registered
     * @return the modeled response
     */
    ActionModel<Cuisine> registerCuisine(final Cuisine cuisine) {
        ActionModel<Cuisine> modeled = CommonParameters.notNullElseFail(cuisine, "NullCuisine");

        return modeled.invoke(Cuisine::getId, this.cuisineRegistry::put)
                      .invoke(RestaurantRegistry::logRegistered);
    }

    /**
     * Registers a collection of {@link Restaurant} with this registry.
     * The registry will map each {@link Restaurant} to cuisine. If no cuisine is mapped, the model fails
     * @param restaurants the collection of restaurants to be registered
     * @return the result of the modeled {@link RestaurantWithCuisine}
     */
    List<ActionModel<RestaurantWithCuisine>> registerRestaurants(final Collection<Restaurant> restaurants) {
        return restaurants.stream().map(this::registerRestaurant).collect(Collectors.toList());
    }

    /**
     * Registers a single {@link Restaurant} with this registry.
     * The registry will map the {@link Restaurant} to a cuisine. If no cuisine is mapped, the model fails
     * @param restaurant the restaurant to be registered
     * @return the result of the modeled {@link RestaurantWithCuisine}
     */
    ActionModel<RestaurantWithCuisine> registerRestaurant(final Restaurant restaurant) {
        final ActionModel<Restaurant> modeledRestaurant = CommonParameters.notNullElseFail(restaurant, "NullRestaurant");

        // Get the mapped cuisine
        final ActionModel<Cuisine> mappedCuisine = modeledRestaurant.map(r -> this.cuisineRegistry.get(r.getCuisineId()));

        // Initialize the model as a failure
        ActionModel<RestaurantWithCuisine> mapped = ActionModel.failure(new Failure("NoMappedCuisine"));

        if (mappedCuisine.hasItem()) {
            // If there is actually a mapped cuisine, create a new RestaurantWithCuisine given the provided data
            mapped = mappedCuisine.map(cuisine -> new RestaurantWithCuisine(restaurant, cuisine));
        }

        return mapped.invoke(this.restaurants::add)
                     .invoke(RestaurantRegistry::logRegistered);
    }

    private static void logRegistered(final CsvRow registered) {
        LOGGER.info("Successfully Registered {}", registered);
    }
}
