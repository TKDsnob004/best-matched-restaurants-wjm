package com.finder.registry;

import com.finder.api.Cuisine;
import com.finder.api.Restaurant;
import com.finder.api.RestaurantWithCuisine;
import com.finder.api.modeling.ActionModel;
import com.finder.utilities.CommonParameters;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A Provider interface that allows for static and/or non-static retrieval of registered restaurant data
 */
@Singleton
public final class RestaurantProvider {

    private static RestaurantRegistry instance = null;

    private static RestaurantRegistry getInstance() {
        RestaurantRegistry temp = instance;
        if (temp == null) {
            temp = new RestaurantRegistry();
            instance = temp;
        }
        return temp;
    }

    /**
     * Clears the current instance
     */
    public static void clear() {
        instance = null;
    }

    /**
     * Registers multiple {@link Cuisine} with the {@link RestaurantRegistry}
     * @param cuisines the cuisines to be registered
     */
    public static void registerMultipleCuisines(final Collection<Cuisine> cuisines) {
        getInstance().registerCuisines(cuisines);
    }

    /**
     * Registers a single {@link Cuisine} with the {@link RestaurantRegistry}
     * @param cuisine the cuisine to be registered
     * @return the modeled results
     */
    public static ActionModel<Cuisine> registerSingleCuisine(final Cuisine cuisine) {
        return getInstance().registerCuisine(cuisine);
    }

    /**
     * Registers multiple {@link Restaurant} with the {@link RestaurantRegistry}
     * @param restaurants the restaurants to be registered
     */
    public static void registerMultipleRestaurants(final Collection<Restaurant> restaurants) {
        getInstance().registerRestaurants(restaurants);
    }

    /**
     * Registers a single {@link Restaurant} with the {@link RestaurantRegistry}
     * @param restaurant the restaurant to be registered
     * @return the modeled results
     */
    public static ActionModel<RestaurantWithCuisine> registerSingleRestaurant(final Restaurant restaurant) {
        return getInstance().registerRestaurant(restaurant);
    }

    /**
     * @return retrieves all registered {@link RestaurantWithCuisine}
     */
    public static List<RestaurantWithCuisine> getAllRegisteredRestaurants() {
        return new ArrayList<>(getInstance().restaurants);
    }

    /**
     * @return retrieves all registered {@link Cuisine}
     */
    public static List<Cuisine> getAllRegisteredCuisines() {
        return new ArrayList<>(getInstance().cuisineRegistry.values());
    }

    /**
     * Finds a list of cuisines given a partial/complete name
     * @param possibleName the partial/complete name for a cuisine
     * @return the modeled list of potential cuisines
     */
    public static ActionModel<List<Cuisine>> findCuisine(final String possibleName) {
        ActionModel<String> modeledName = CommonParameters.notNullElseFail(possibleName, "NullId")
                                                          .map(String::toUpperCase);
        final List<Cuisine> registered = getAllRegisteredCuisines();

        return modeledName.map(name -> registered.stream()
                                                 .filter(c -> c.getName().toUpperCase().contains(name))
                                                 .collect(Collectors.toList()));
    }

    /**
     * Finds a matching cuisine given an optional cuisine id
     * @param cuisineId the unique id of the cuisine
     * @return the modeled potential match
     */
    public static ActionModel<Cuisine> findCuisine(final Long cuisineId) {
        ActionModel<Long> modeledId = CommonParameters.notNullElseFail(cuisineId, "NullId");

        final List<Cuisine> cuisines = getAllRegisteredCuisines();
        return modeledId.map(id -> cuisines.stream()
                                           .filter(c -> id.equals(c.getId()))
                                           .findFirst()
                                           .orElse(null));
    }

    /**
     * Private constructor
     */
    private RestaurantProvider() {
        // Empty
    }
}
