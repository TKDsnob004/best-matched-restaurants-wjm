package com.finder.app;

import com.finder.api.Query;
import com.finder.api.RestaurantWithCuisine;
import com.finder.registry.RestaurantProvider;
import io.smallrye.mutiny.Uni;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * The application scoped working bean
 */
@ApplicationScoped
public class Worker {

    private final ConfigurationProvider provider;

    /**
     * Injectable constructor
     * @param provider the configuration provider
     */
    @Inject
    public Worker(final ConfigurationProvider provider) {
        this.provider = requireNonNull(provider, "the provider is required");
    }

    /**
     * Finds all restaurants provided a possible cuisine type and a query builder
     * @param possibleCuisine the possible type of cuisine to be used
     * @param builder the query builder
     * @return the list of {@link RestaurantWithCuisine} provided a query
     */
    List<RestaurantWithCuisine> findRestaurants(final String possibleCuisine, final Query.QueryBuilder builder) {
        RestaurantProvider.findCuisine(possibleCuisine).invoke(builder::addAllCuisines);

        return findRestaurants(builder.build());
    }

    /**
     * Finds all restaurants provided a cuisine id
     * @param cuisineId the unique id of the cuisine
     * @param builder the query builder
     * @return the list of {@link RestaurantWithCuisine} provided a query
     */
    List<RestaurantWithCuisine> findRestaurants(final Long cuisineId, final Query.QueryBuilder builder) {
        RestaurantProvider.findCuisine(cuisineId).invoke(builder::addCuisine);

        return findRestaurants(builder.build());
    }

    /**
     * Finds all restaurants provided a {@link Query}
     * @param query the built query
     * @return the list of {@link RestaurantWithCuisine} provided a query
     */
    List<RestaurantWithCuisine> findRestaurants(final Query query) {
        return query.query(this.provider.maxItems(), RestaurantProvider.getAllRegisteredRestaurants());
    }
}
