package com.finder.app;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;
import io.smallrye.config.WithName;

/**
 * This services configurable information
 */
@ConfigMapping(prefix = "finder")
public interface FinderConfiguration {

    /**
     * @return the path to the cuisines.csv file.
     */
    @WithName("cuisine-location")
    @WithDefault("src/main/resources/csv/cuisines.csv")
    String getCuisineLocation();

    /**
     * @return the path to the restaurants.csv file
     */
    @WithName("restaurants-location")
    @WithDefault("src/main/resources/csv/restaurants.csv")
    String getRestaurantsLocation();

    /**
     * @return the maximum number of items to be returned. Defaults to 5
     */
    @WithName("max-items")
    @WithDefault("5")
    long getMaxItems();
}
