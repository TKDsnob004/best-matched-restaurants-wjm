package com.finder.app;

import com.finder.api.Cuisine;
import com.finder.api.Restaurant;
import com.finder.registry.RestaurantProvider;
import com.finder.utilities.csv.CsvFileReader;
import io.quarkus.runtime.StartupEvent;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.io.InputStream;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * ApplicationScoped Bean that provided the configured csv files to the internal registry.
 */
@ApplicationScoped
public class ConfigurationProvider {

    private final FinderConfiguration serviceConfig;

    /**
     * Injectable constructor
     * @param serviceConfig this services configurable data
     */
    @Inject
    public ConfigurationProvider(final FinderConfiguration serviceConfig) {
        this.serviceConfig = requireNonNull(serviceConfig, "a service configuration is required");
    }

    /**
     * The on start event
     * @param ev the Quarkus startup event
     */
    void onStart(@Observes final StartupEvent ev) {
        registerCuisines(getStream(this.serviceConfig.getCuisineLocation()));
        loadRestaurants(getStream(this.serviceConfig.getRestaurantsLocation()));
    }

    /**
     * @return the maximum number of items this service responds with
     */
    public long maxItems() {
        return this.serviceConfig.getMaxItems();
    }

    /**
     * Registers the csv file containing the list of cuisines with this service
     */
    static void registerCuisines(final InputStream stream) {
        final List<Cuisine> readCuisines = CsvFileReader.read(stream, Cuisine::read);
        RestaurantProvider.registerMultipleCuisines(readCuisines);
    }

    /**
     * Registers the csv file containing a list of restaurants with this service
     */
    static void loadRestaurants(final InputStream stream) {
        final List<Restaurant> readRestaurants = CsvFileReader.read(stream, Restaurant::read);
        RestaurantProvider.registerMultipleRestaurants(readRestaurants);
    }

    /**
     * Returns an input stream given a provided path to a csv file
     * @param path the path to the csv file (located in the resources folder)
     * @return the input stream provided
     */
    private InputStream getStream(final String path) {
        return ConfigurationProvider.class.getClassLoader().getResourceAsStream(path);
    }
}
