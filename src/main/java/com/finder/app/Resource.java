package com.finder.app;

import com.finder.api.Cuisine;
import com.finder.api.Query;
import com.finder.api.Restaurant;
import com.finder.api.RestaurantWithCuisine;
import com.finder.api.modeling.Failure;
import com.finder.api.modeling.ActionModel;
import com.finder.registry.RestaurantProvider;
import io.smallrye.mutiny.Uni;
import org.eclipse.microprofile.context.ManagedExecutor;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.function.Supplier;

/**
 * REST resource used to query information contained in this service
 */
@RequestScoped
@Path("v1/finder")
public class Resource {

    @Inject
    Worker worker;

    @Inject
    ManagedExecutor executor;

    /**
     * Retrieves a list of {@link RestaurantWithCuisine} based on the provided query parameters
     * @param name the unique name of the restaurant
     * @param rating the restaurants rating
     * @param price the average price
     * @param distance the distance to the restaurant
     * @param cuisineName the unique id of the cuisine
     * @return the list of {@link RestaurantWithCuisine} matching the data provided
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<ActionModel<List<RestaurantWithCuisine>>> findMatchingFromName(@QueryParam("name") final String name,
                                                                              @QueryParam("rating") final Integer rating,
                                                                              @QueryParam("price") final Double price,
                                                                              @QueryParam("distance") final Double distance,
                                                                              @QueryParam("cuisineName") final String cuisineName) {
        final Query.QueryBuilder builder = Query.create()
                                                .setAveragePrice(price)
                                                .setRestaurantName(name)
                                                .setDistance(distance)
                                                .setCustomerRating(rating);
        return runAsync(() -> worker.findRestaurants(cuisineName, builder));
    }

    /**
     * Retrieves a list of {@link RestaurantWithCuisine} based on the provided query parameters
     * @param name the unique name of the restaurant
     * @param rating the restaurants rating
     * @param price the average price
     * @param distance the distance to the restaurant
     * @param cuisineId the unique id of the cuisine
     * @return the list of {@link RestaurantWithCuisine} matching the data provided
     */
    @GET
    @Path("/{cuisineId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<ActionModel<List<RestaurantWithCuisine>>> findMatchingFromId(@QueryParam("name") final String name,
                                                                            @QueryParam("rating") final Integer rating,
                                                                            @QueryParam("price") final Double price,
                                                                            @QueryParam("distance") final Double distance,
                                                                            @PathParam("cuisineId") final long cuisineId) {
        final Query.QueryBuilder builder = Query.create()
                                                .setAveragePrice(price)
                                                .setRestaurantName(name)
                                                .setDistance(distance)
                                                .setCustomerRating(rating);
        return runAsync(() -> worker.findRestaurants(cuisineId, builder));
    }

    /**
     * @return the registered cuisines
     */
    @GET
    @Path("/cuisine")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<ActionModel<List<Cuisine>>> getConfiguredCuisines() {
        return runAsync(RestaurantProvider::getAllRegisteredCuisines);
    }

    /**
     * Upserts an additional {@link Cuisine}
     * @param id the unique id of the cuisine
     * @param cuisine the cuisine to be upserted
     * @return the modeled response for this update
     */
    @PUT
    @Path("/cuisine/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ActionModel<Cuisine> registerNewCuisine(@PathParam("id") final long id, final Cuisine cuisine) {
        if (id != cuisine.getId()) {
            return ActionModel.failure(new Failure("MismatchedIds"));
        }

        return RestaurantProvider.registerSingleCuisine(cuisine);
    }

    /**
     * @param limit limits the number of returned results. If limit == 0, there is no limit
     * @return the list of configured {@link RestaurantWithCuisine}
     */
    @GET
    @Path("/restaurant")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<ActionModel<List<RestaurantWithCuisine>>> getConfiguredRestaurants(@QueryParam("limit") long limit) {
        return runAsync(() -> Query.empty().query(limit, RestaurantProvider.getAllRegisteredRestaurants()));
    }

    /**
     * Adds another restaurant to the currently registered list
     * @param restaurant the restaurant to be added
     * @return the modeled response for this addition
     */
    @PUT
    @Path("/restaurant")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public ActionModel<RestaurantWithCuisine> registerNew(final Restaurant restaurant) {
        return RestaurantProvider.registerSingleRestaurant(restaurant);
    }


    /**
     * Runs a operation asynchronously
     * @param supplier the supplier
     * @return the async operation
     */
    private <T> Uni<ActionModel<List<T>>> runAsync(final Supplier<List<T>> supplier) {
        return Uni.createFrom().completionStage(executor.supplyAsync(supplier))
                .onItemOrFailure()
                    .transform((queried, t) -> {
                        if (t != null) {
                            return ActionModel.failure(t);
                        }
                        return ActionModel.success(queried);
                    });
    }
}
