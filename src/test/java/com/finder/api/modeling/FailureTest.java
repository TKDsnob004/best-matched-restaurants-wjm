package com.finder.api.modeling;

import org.junit.jupiter.api.Test;

import javax.json.Json;
import javax.json.bind.Jsonb;
import javax.json.bind.spi.JsonbProvider;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit tests for {@link Failure}
 */
public class FailureTest {
    private static final Jsonb jsonb = JsonbProvider.provider().create().build();

    @Test
    void constructor() {
        final String code = "NewCode";

        final Failure failure = new Failure(code);

        assertEquals(code, failure.getCode());

        final Failure noFailure = Failure.NO_FAILURE;

        assertEquals("NoFailure", noFailure.getCode());
    }

    @Test
    void json() {
        final String code = "NewCode";

        final Failure failure = new Failure(code);

        final String expectedJson = Json.createObjectBuilder().add("code", code).build().toString();
        final String actualJson = jsonb.toJson(failure);

        assertEquals(expectedJson, actualJson);
    }
}
