package com.finder.api;

import com.finder.app.ConfigurationProviderTest;
import com.finder.registry.RestaurantProvider;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Unit tests for {@link Query}
 */
public class QueryTest {

    @Test
    void verifyLimiter() {
        ConfigurationProviderTest.reRegisterProvider();

        final Query empty = Query.create().build();
        final long limit = 10;
        List<RestaurantWithCuisine> topFive = empty.query(limit, RestaurantProvider.getAllRegisteredRestaurants());

        assertEquals(limit, topFive.size());
    }

    @Test
    void verifyFromName() {
        final long cuisineId = 0;

        final String queryName = "test";

        final String firstName = "testing";
        final String secondName = "tester";
        final String ignoredName = "nothing";

        final Cuisine cuisine = new Cuisine(cuisineId, "name");

        final Restaurant one = new Restaurant(firstName, 5, 1d, 10d, cuisineId);
        final Restaurant two = new Restaurant(firstName, 4, 1d, 10d, cuisineId);
        final Restaurant three = new Restaurant(secondName, 3, 1d, 10d, cuisineId);
        final Restaurant ignored = new Restaurant(ignoredName, 2, 1d, 10d, cuisineId);

        final RestaurantWithCuisine first =  new RestaurantWithCuisine(one, cuisine);
        final RestaurantWithCuisine second = new RestaurantWithCuisine(two, cuisine);
        final RestaurantWithCuisine third = new RestaurantWithCuisine(three, cuisine);
        final RestaurantWithCuisine filtered = new RestaurantWithCuisine(ignored, cuisine);

        final List<RestaurantWithCuisine> registered = List.of(first, second, third, filtered);
        final List<RestaurantWithCuisine> expected = List.of(first, second, third);

        final Query query = Query.create().setRestaurantName(queryName).build();

        List<RestaurantWithCuisine> actual = query.query(4, registered);

        assertEquals(expected, actual);
    }

    @Test
    void verifyFromRating() {
        final long cuisineId = 0;

        final int queryRating = 4;

        final int firstRating = 5;
        final int secondRating = 4;
        final int ignoredRating = 3;

        final Cuisine cuisine = new Cuisine(cuisineId, "name");

        final Restaurant one = new Restaurant("first", firstRating, 1d, 10d, cuisineId);
        final Restaurant two = new Restaurant("second", secondRating, 2d, 10d, cuisineId);
        final Restaurant ignored = new Restaurant("ignored", ignoredRating, 1d, 10d, cuisineId);

        final RestaurantWithCuisine first =  new RestaurantWithCuisine(one, cuisine);
        final RestaurantWithCuisine second = new RestaurantWithCuisine(two, cuisine);
        final RestaurantWithCuisine filtered = new RestaurantWithCuisine(ignored, cuisine);

        final List<RestaurantWithCuisine> registered = List.of(first, second, filtered);
        final List<RestaurantWithCuisine> expected = List.of(first, second);

        final Query query = Query.create().setCustomerRating(queryRating).build();

        List<RestaurantWithCuisine> actual = query.query(3, registered);

        assertEquals(expected, actual);
    }

    @Test
    void verifyFromDistance() {
        final long cuisineId = 0;

        final double queryDistance = 20d;

        final double firstDistance = 1d;
        final double secondDistance = 10d;
        final double ignoredDistance = queryDistance + secondDistance;

        final Cuisine cuisine = new Cuisine(cuisineId, "name");

        final Restaurant one = new Restaurant("first", 5, firstDistance, 10d, cuisineId);
        final Restaurant two = new Restaurant("second", 5, secondDistance, 10d, cuisineId);
        final Restaurant ignored = new Restaurant("ignored", 5, ignoredDistance, 10d, cuisineId);

        final RestaurantWithCuisine first =  new RestaurantWithCuisine(one, cuisine);
        final RestaurantWithCuisine second = new RestaurantWithCuisine(two, cuisine);
        final RestaurantWithCuisine filtered = new RestaurantWithCuisine(ignored, cuisine);

        final List<RestaurantWithCuisine> registered = List.of(first, second, filtered);
        final List<RestaurantWithCuisine> expected = List.of(first, second);

        final Query query = Query.create().setDistance(queryDistance).build();

        List<RestaurantWithCuisine> actual = query.query(3, registered);

        assertEquals(expected, actual);
    }

    @Test
    void verifyFromPrice() {
        final long cuisineId = 0;

        final double queryPrice = 10d;

        final double firstPrice = 1d;
        final double secondPrice = 5d;
        final double ignoredPrice = queryPrice + firstPrice;

        final Cuisine cuisine = new Cuisine(cuisineId, "name");

        final Restaurant one = new Restaurant("first", 5, 1d, firstPrice, cuisineId);
        final Restaurant two = new Restaurant("second", 5, 1d, secondPrice, cuisineId);
        final Restaurant ignored = new Restaurant("ignored", 5, 1d, ignoredPrice, cuisineId);

        final RestaurantWithCuisine first =  new RestaurantWithCuisine(one, cuisine);
        final RestaurantWithCuisine second = new RestaurantWithCuisine(two, cuisine);
        final RestaurantWithCuisine filtered = new RestaurantWithCuisine(ignored, cuisine);

        final List<RestaurantWithCuisine> registered = List.of(first, second, filtered);
        final List<RestaurantWithCuisine> expected = List.of(first, second);

        final Query query = Query.create().setAveragePrice(queryPrice).build();

        List<RestaurantWithCuisine> actual = query.query(3, registered);

        assertEquals(expected, actual);
    }

    @Test
    void verifyFromCuisines() {
        RestaurantProvider.clear();

        final long cuisineId = 1;
        final long firstCuisineId = 2;
        final long ignoredCuisineId = cuisineId + firstCuisineId;

        final Cuisine queryCuisineOne = new Cuisine(cuisineId, "name");
        final Cuisine queryCuisineTwo = new Cuisine(firstCuisineId, "name-two");

        final Cuisine ignoredCuisine = new Cuisine(ignoredCuisineId, "different");

        final Restaurant one = new Restaurant("first", 5, 1d, 1d, cuisineId);
        final Restaurant two = new Restaurant("second", 5, 1d, 2d, firstCuisineId);
        final Restaurant ignored = new Restaurant("ignored", 5, 1d, 3d, ignoredCuisineId);

        final RestaurantWithCuisine first =  new RestaurantWithCuisine(one, queryCuisineOne);
        final RestaurantWithCuisine second = new RestaurantWithCuisine(two, queryCuisineTwo);
        final RestaurantWithCuisine filtered = new RestaurantWithCuisine(ignored, ignoredCuisine);

        final List<RestaurantWithCuisine> registered = List.of(first, second, filtered);
        final List<RestaurantWithCuisine> expected = List.of(first, second);

        final Query query = Query.create().addCuisine(queryCuisineOne).addCuisine(queryCuisineTwo).build();

        List<RestaurantWithCuisine> actual = query.query(3, registered);

        assertEquals(expected, actual);
    }

    @Test
    void verifyGetRegisteredCuisines() {
        RestaurantProvider.clear();

        final String queryName = "test";

        final String cuisineOneName = queryName + "one";
        final String cuisineTwoName = queryName.toUpperCase() + "two";
        final String cuisineIgnoredName = "ignored";

        final long firstCuisineId = 1;
        final long secondCuisineId = 2;
        final long ignoredCuisineId = firstCuisineId + secondCuisineId;

        final Cuisine queryCuisineOne = new Cuisine(firstCuisineId, cuisineOneName);
        final Cuisine queryCuisineTwo = new Cuisine(secondCuisineId, cuisineTwoName);
        final Cuisine ignoredCuisine = new Cuisine(ignoredCuisineId, cuisineIgnoredName);

        RestaurantProvider.registerMultipleCuisines(List.of(queryCuisineOne, queryCuisineTwo, ignoredCuisine));

        final Restaurant one = new Restaurant("first", 5, 1d, 1d, firstCuisineId);
        final Restaurant two = new Restaurant("second", 5, 1d, 2d, secondCuisineId);
        final Restaurant ignored = new Restaurant("ignored", 5, 1d, 3d, ignoredCuisineId);

        final RestaurantWithCuisine first =  new RestaurantWithCuisine(one, queryCuisineOne);
        final RestaurantWithCuisine second = new RestaurantWithCuisine(two, queryCuisineTwo);
        final RestaurantWithCuisine filtered = new RestaurantWithCuisine(ignored, ignoredCuisine);

        final List<Cuisine> matchingCuisines = RestaurantProvider.findCuisine(queryName).getItem();

        final List<RestaurantWithCuisine> registered = List.of(first, second, filtered);
        final List<RestaurantWithCuisine> expected = List.of(first, second);

        final Query query = Query.create().addAllCuisines(matchingCuisines).build();

        List<RestaurantWithCuisine> actual = query.query(3, registered);

        assertEquals(expected, actual);
    }

    @Test
    void verifyAllParameters() {

        /**
         * Sorts a list of {@link RestaurantWithCuisine} by distance (closest -> furthest),
         *                                               then by number of stars (higher -> lower)
         *                                               then by price (lowest -> highest)
         *                                               and finally by the name of the mapped cuisine
         */

        final double queryDistance = 5d;
        final int queryNumStars = 4;
        final double queryPrice = 3d;

        final long firstCuisineId = 1;
        final long secondCuisineId = 2;

        final Cuisine queryCuisineOne = new Cuisine(firstCuisineId, "abcd");
        final Cuisine queryCuisineTwo = new Cuisine(secondCuisineId, "bcde");

        final Restaurant one = new Restaurant("first", queryNumStars, queryDistance - 1d, queryPrice, firstCuisineId);
        final Restaurant two = new Restaurant("second", 5, queryDistance, queryPrice, firstCuisineId);
        final Restaurant three = new Restaurant("third", queryNumStars, queryDistance, queryPrice - 1d, firstCuisineId);
        final Restaurant four = new Restaurant("forth", queryNumStars, queryDistance, queryPrice, firstCuisineId);
        final Restaurant five = new Restaurant("fifth", queryNumStars, queryDistance, queryPrice, secondCuisineId);
        final Restaurant ignoredOne = new Restaurant("ignoredOne", queryNumStars - 1, queryDistance, queryPrice, secondCuisineId);
        final Restaurant ignoredTwo = new Restaurant("ignoredTwo", queryNumStars, queryDistance + 1, queryPrice, secondCuisineId);
        final Restaurant ignoredThree = new Restaurant("ignoredThree", queryNumStars, queryDistance, queryPrice - 1d, secondCuisineId);

        RestaurantProvider.registerMultipleCuisines(List.of(queryCuisineOne, queryCuisineTwo));


    }
}
