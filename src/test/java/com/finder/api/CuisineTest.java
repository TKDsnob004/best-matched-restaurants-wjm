package com.finder.api;

import org.junit.jupiter.api.Test;

import javax.json.bind.Jsonb;
import javax.json.bind.spi.JsonbProvider;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

/**
 * Unit tests for {@link Cuisine}
 */
public class CuisineTest {
    private static final Jsonb jsonb = JsonbProvider.provider().create().build();

    @Test
    void constructor() {
        final long id = 0;
        final String name = "name";

        final Cuisine cuisine = new Cuisine(id, name);

        assertEquals(id, cuisine.getId());
        assertEquals(name, cuisine.getName());
    }

    @Test
    void jsonb() {
        final Cuisine cuisine = new Cuisine(0, "name");

        final String json = jsonb.toJson(cuisine);
        final Cuisine back = jsonb.fromJson(json, Cuisine.class);

        assertEquals(cuisine, back);
    }

    @Test
    void equals() {
        final long id = 0;
        final String name = "name";
        final Cuisine cuisine = new Cuisine(id, name);
        final Cuisine same = new Cuisine(id, name);
        final Cuisine diffId = new Cuisine(id + 1, name);
        final Cuisine diffName = new Cuisine(id, name + "different");

        assertEquals(cuisine, cuisine);
        assertEquals(cuisine, same);
        assertNotEquals(cuisine, null);
        assertNotEquals(null, cuisine);
        assertNotEquals(cuisine, diffId);
        assertNotEquals(cuisine, diffName);
        assertNotEquals(cuisine, "different");
    }
}
