package com.finder.app;

import com.finder.api.Cuisine;
import com.finder.api.Query;
import com.finder.api.Restaurant;
import com.finder.api.RestaurantWithCuisine;
import com.finder.registry.RestaurantProvider;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link Worker}
 */
@ExtendWith(MockitoExtension.class)
public class WorkerTest {

    @Mock
    ConfigurationProvider provider;

    @InjectMocks
    Worker worker;

    @Test
    void verifyAllParameters() {
        final String queryName = "query";
        final double queryDistance = 5d;
        final int queryNumStars = 4;
        final double queryPrice = 3d;

        final long firstCuisineId = 1;
        final long secondCuisineId = 2;

        final Cuisine queryCuisineOne = new Cuisine(firstCuisineId, "abcd");
        final Cuisine queryCuisineTwo = new Cuisine(secondCuisineId, "bcde");

        final Restaurant one = new Restaurant(queryName + "first", queryNumStars, queryDistance - 1d,
                                                    queryPrice, firstCuisineId);

        final Restaurant two = new Restaurant(queryName + "second", 5, queryDistance, queryPrice,
                                                    firstCuisineId);

        final Restaurant three = new Restaurant(queryName + "third", queryNumStars, queryDistance,
                                                queryPrice - 1d, firstCuisineId);

        final Restaurant four = new Restaurant(queryName + "forth", queryNumStars, queryDistance, queryPrice,
                                                     firstCuisineId);

        final Restaurant five = new Restaurant(queryName + "fifth", queryNumStars, queryDistance, queryPrice,
                                                     secondCuisineId);

        final Restaurant ignoredOne = new Restaurant(queryName + "ignoredOne", queryNumStars - 1,
                                                           queryDistance, queryPrice, secondCuisineId);

        final Restaurant ignoredTwo = new Restaurant("ignoredTwo", queryNumStars, queryDistance + 1,
                                                            queryPrice, secondCuisineId);

        final Restaurant ignoredThree = new Restaurant(queryName + "ignoredThree", queryNumStars, queryDistance,
                                                        queryPrice + 1, secondCuisineId);

        RestaurantProvider.registerMultipleCuisines(List.of(queryCuisineOne, queryCuisineTwo));
        RestaurantProvider.registerMultipleRestaurants(List.of(one, two, three, four, five, ignoredOne, ignoredTwo,
                                                               ignoredThree));

        final List<Restaurant> expected = List.of(one, two, three, four, five);
        final Query query = Query.create()
                                 .addCuisine(queryCuisineOne)
                                 .addCuisine(queryCuisineTwo)
                                 .setAveragePrice(queryPrice)
                                 .setCustomerRating(queryNumStars)
                                 .setDistance(queryDistance)
                                 .setRestaurantName(queryName).build();

        when(provider.maxItems()).thenReturn(5L);

        final List<RestaurantWithCuisine> queried = worker.findRestaurants(query);

        for (int i = 0; i < expected.size(); i++) {
            assertEquals(expected.get(i), queried.get(i));
        }
    }
}
