package com.finder.app;

import com.finder.registry.RestaurantProvider;
import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * Unit tests for {@link ConfigurationProvider}
 */
public class ConfigurationProviderTest {

    private static final String CUISINE_PATH = "/csv/cuisines.csv";
    private static final String RESTAURANT_PATH = "/csv/restaurants.csv";

    @Test
    void verifyLocations() {
        final InputStream cuisineStream = getStream(CUISINE_PATH);
        final InputStream restaurantStream = getStream(RESTAURANT_PATH);

        ConfigurationProvider.registerCuisines(cuisineStream);
        ConfigurationProvider.loadRestaurants(restaurantStream);

        assertFalse(RestaurantProvider.getAllRegisteredCuisines().isEmpty());
        assertFalse(RestaurantProvider.getAllRegisteredRestaurants().isEmpty());
    }

    /**
     * Registers the provider for other tests
     */
    public static void reRegisterProvider() {
        RestaurantProvider.clear();

        final InputStream cuisineStream = getStream(CUISINE_PATH);
        final InputStream restaurantStream = getStream(RESTAURANT_PATH);

        ConfigurationProvider.registerCuisines(cuisineStream);
        ConfigurationProvider.loadRestaurants(restaurantStream);
    }

    static InputStream getStream(final String path) {
        return ConfigurationProviderTest.class.getResourceAsStream(path);
    }
}
